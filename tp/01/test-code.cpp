#include <cstring>
#include "catch.hpp"
#include "funciones.h"

TEST_CASE ("Nombre corto") {
	char* ptr;
	// usuario tiene 7 caracteres
	char nombre[] = "usuario";

	ptr = nombre;

	REQUIRE ( nombreUsuarioValido (ptr) == true );
}

TEST_CASE ("Nombre largo") {
	char* ptr;
	// usuarioCon16Cara tiene 16 caracteres
	char nombre[] = "usuarioCon16Cara";

	ptr = nombre;

	REQUIRE ( nombreUsuarioValido (ptr) == false );
}

TEST_CASE ("Email con arroba") {
	char* ptr;
	char email[] = "corto@mail.com";

	ptr = email;

	REQUIRE ( emailUsuarioValido (ptr) == true );
}

TEST_CASE ("Email sin arroba") {
	char* ptr;
	char email[] = "cortoarroba.com";

	ptr = email;

	REQUIRE ( emailUsuarioValido (ptr) == false );
}

TEST_CASE ("Password valida") {
	char* ptr;
	char passwd[] = "Aa2341zz";

	ptr = passwd;

	REQUIRE ( passwordUsuarioValida (ptr) == true );
}

TEST_CASE ("Password corta") {
	char* ptr;
	char passwd[] = "Aa23";

	ptr = passwd;

	REQUIRE ( passwordUsuarioValida (ptr) == false );
}

TEST_CASE ("Password no valida") {
	char* ptr;
	char passwd[] = "Aa23!aaa";

	ptr = passwd;

	REQUIRE ( passwordUsuarioValida (ptr) == false );
}

TEST_CASE ("Testing generar dat") {
	generarDat();

	REQUIRE ( true == true );
}

TEST_CASE ("Testing ordenar dat") {
	ordenarUsuarios();

	REQUIRE ( true == true );
}

TEST_CASE ("Testing de buscar usuario por email") {
	char email[] = "cort0@aol.org";
	int a = buscarUsuarioPorMail(email);

	printf("Encontrado en %d\n", a);

	REQUIRE ( true == true );
}

TEST_CASE ("Testing login de usuario valido") {
	char email[] = "cort0@aol.org";
	char passwd[] = "A123456790";

	REQUIRE ( login(email, passwd) == true );
}

TEST_CASE ("Testing login de usuario fallido") {
	char email[] = "cort0@aol.org";
	char passwd[] = "A123411790";

	REQUIRE ( login(email, passwd) == false );
}

TEST_CASE ("Testing agregar usuario no existente") {
	char nombre[] = "leo";
	char email[] = "leo@aol.org";
	char passwd[] = "A123411790";

	agregarUsuario(nombre, email, passwd);

	REQUIRE ( true == true );
}

TEST_CASE ("Testing agregar usuario existente") {
	char nombre[] = "leo";
	char email[] = "leo1@aol.org";
	char passwd[] = "A123411790";

	agregarUsuario(nombre, email, passwd);

	REQUIRE ( true == true );
}
