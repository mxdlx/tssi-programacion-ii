#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;

struct Fecha {
	int dia;
	int mes;
	int anio;
};

struct Inscripcion {
	char apenom[26];
	int legajo;
	int codigo_materia;
	Fecha fecha;
};

int main () {
	// No se entiende una goma la descripcion del ejercicio pero la gracia es
	// hacer un append de un archivo "novedades" a un archivo de cosas "anteriores".
	//
	// En la clase le agregaron contar los registros del archivo de "anteriores" con ftell y fseek con esto:
	// int cantRegistros(FILE *file) {
	//	int pos = ftell(file);
	//	fseek(file, 0, SEEK_END);
	//	int posFinal = ftell(file);
	//	fseek(file, pos, SEEK_SET);
	//	return posFinal / sizeof(Inscripcion);
	// }
	//
	// Tambien imprimen solo las novedades a partir del archivo de anteriores guardando la posicion inicial antes de escribir
	// pero es un ftell mas.
	FILE* anteriores_ptr;
	FILE* novedades_ptr;

	Inscripcion i;
	strcpy(i.apenom, "Juan Carlos");
	i.legajo = 80010;
	i.codigo_materia = 100205;
	Fecha f_1{22,5,2020};
	i.fecha = f_1;

	Inscripcion j;
	strcpy(j.apenom, "Juan Cruz");
	j.legajo = 80105;
	j.codigo_materia = 100205;
	Fecha f_2{22,5,2020};
	j.fecha = f_2;

	if ((anteriores_ptr = fopen("anteriores.dat", "wb")) == NULL) {
		printf("Error al abrir el archivo anteriores.dat");
		exit(EXIT_FAILURE);
	}

	fwrite(&i, sizeof(Inscripcion), 1, anteriores_ptr);
	fwrite(&j, sizeof(Inscripcion), 1, anteriores_ptr);
	fclose(anteriores_ptr);

	if ((anteriores_ptr = fopen("anteriores.dat", "ab+")) == NULL) {
		printf("Error al abrir el archivo de anteriores\n");
		exit(EXIT_FAILURE);
	}

	if ((novedades_ptr = fopen("../ej07/ej07.dat", "rb")) == NULL) {
		printf("Error al abrir el archivo de novedades\n");
		exit(EXIT_FAILURE);
	}

	int r = fread(&i, sizeof(Inscripcion), 1, novedades_ptr);

	if (r == 0) {
		printf("No hay novedades\n");
	}

	while (!feof(novedades_ptr)) {
		fwrite(&i, sizeof(Inscripcion), 1, anteriores_ptr);
		fread(&i, sizeof(Inscripcion), 1, novedades_ptr);
	}

	return 0;
}
