#include <iostream>
#include <cstring>
using namespace std;

class Alumno {
	private:
		int legajo;
		char nombre[26];
		int nota;
	public:
		void setLegajo (int l) {
			if (l > 9999999 && l < 100000000) {
				legajo = l;
			} else {
				printf("[ERROR] El legajo va de 10000000 a 99999999\n");
			}
		}
		int getLegajo () {
			return legajo;
		}

		void setNombre (char* n) {
			strcpy(nombre, n);
		}
		char* getNombre () {
			return nombre;
		}

		void setNota (int n) {
			if (nota >= 0 && nota < 11) {
				nota = n;
			} else {
				printf("[ERROR] La nota va de 0 a 10\n");
			}
		}
		int getNota () {
			return nota;
		}
};

int main () {
	Alumno a;
	a.setLegajo(12345678);
	printf("[INFO] El legajo del alumno es %d\n", a.getLegajo());

	char nombre[] = "Juan Carlos";
	a.setNombre(nombre);
	char *ptr;
	ptr = a.getNombre();
	printf("[INFO] El nombre del alumno es %s\n", ptr);

	a.setNota(7);
	printf("[INFO] La nota del alumno es %d\n", a.getNota());

	return 0;
}
