#include <iostream>
#include <cstring>
using namespace std;

int main () {

	char cadena[80 + 1];
	char output[80 + 1];
	char formato[3];
	char *ptr_coma;
	char *ptr_punto;

	printf(">> Ingrese una valor formateado: ");
	scanf("%[^\n]%*c", cadena);

	printf(">> Ingrese un formato (AR/EN): ");
	scanf("%[^\n]%*c", formato);

	// Para saber si el formato ingresado es uno u otro,
	// voy a tomar el ultimo punto y la ultima coma,
	// el que tenga el valor de puntero mas alto gana.
	ptr_coma = strrchr(cadena, ',');
	ptr_punto = strrchr(cadena, '.');

	int pos_coma = ptr_coma - cadena + 1;
	int pos_punto = ptr_punto - cadena + 1;
	int formato_detectado = 0;

	// Argentino: 1
	// Ingles: 0
	if (pos_coma > pos_punto) {
		formato_detectado = 1;
	}

	// Caso base en el que no es necesario cambiar el formato
	if ((strcmp(formato, "AR") == 0 && formato_detectado == 1) || (strcmp(formato, "EN") == 0 && formato_detectado == 0)) {
		printf("> La conversion no es necesaria\n");
		printf("> Resultado: %s\n", cadena);
	} else {
		// Uso este puntero solo para reutilizar
		int lenCadena = (int) strlen(cadena);
		ptr_punto = strtok(cadena, ".,");

		int offset = 0;

		while (ptr_punto != NULL) {
			for (int i = 0; i < (int) strlen(ptr_punto); i++) {
				*(output + offset + i) = *(ptr_punto + i);
			}

			if (strcmp(formato, "AR") == 0) {
				*(output + offset + (int) strlen(ptr_punto)) = '.';
			} else {
				*(output + offset + (int) strlen(ptr_punto)) = ',';
			}

			offset = offset + (int) strlen(ptr_punto) + 1;

			ptr_punto = strtok(NULL, ".,");
		}

		if (strcmp(formato, "AR") == 0) {
			*(output + pos_punto - 1) = ',';
		} else {
			*(output + pos_coma - 1) = '.';
		}

		*(output + (int) lenCadena) = '\0';
		printf("> Output: %s\n", output);
	}

	return 0;
}
