#include <iostream>
#include <cstring>
using namespace std;

int main () {
	char cadena_a[80+1];
	char cadena_b[40+1];
	char *ptr;

	printf("> Ingrese la cadena: ");
	scanf("%[^\n]%*c", cadena_a);

	printf("> Ingrese la subcadena: ");
	scanf("%[^\n]%*c", cadena_b);

	int len_b = 0;
	while (*(cadena_b + len_b) != '\0') {
		len_b++;
	}

	int indice = 0;
	int sub_indice = 0;
	int contador = 0;
	int equidad = 0;
	int puntero = 0;
	while (*(cadena_a + contador) != '\0') {
		puntero = contador;
		while (*(cadena_a + contador) == *(cadena_b + sub_indice) && *(cadena_a + contador) != '\0') {
			equidad++;
			sub_indice++;
			contador++;
		}

		if (equidad != len_b) {
			equidad = 0;
			sub_indice = 0;
			contador++;
		} else {
			printf("> Resultado: %d\n", puntero);
			break;
		}
	}
	return 0;
}
