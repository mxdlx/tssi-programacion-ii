#include <iostream>

int main () {
	int alto = 0;
	int bajo = 0;
	float promedio = 0;
	int sumador = 0;
	int contador = 0;

	while (true) {
		int valor = 1;

		printf("Ingrese un numero o 0 para salir: ");
		scanf("%d", &valor);

		if (valor == 0) {
			break;
		}

		contador++;

		if (contador == 1) {
			alto = valor;
			bajo = valor;
		}

		sumador += valor;

		if (valor > alto) {
			alto = valor;
		}

		if (valor < bajo) {
			bajo = valor;
		}
	}

	printf("El numero ingresado mas alto fue: %d\n", alto);
	printf("El numero ingresado mas bajo fue: %d\n", bajo);
	printf("El promedio de los numeros ingresados fue: %4.2f\n", float(sumador) / float(contador));
	return 0;
}
