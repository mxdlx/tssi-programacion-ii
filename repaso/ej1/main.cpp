#include "funciones.h"
#include <iostream>

int main () {
	int a[MAX_ELEMENTOS];

	inicializarVector(a);
	printf(">> Vector inicializado: \n");
	imprimirVector(a);
	printf(">> Vector invertido: \n");
	imprimirVectorInvertido(a);
	return 0;
}
