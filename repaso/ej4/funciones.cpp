#include "estructuras.h"
#include <random>
using namespace std;

const string nombresEmpleados[5] = {
	"Juan",
	"Maria",
	"Carlos",
	"Juana",
	"Jose"
};

const string apellidosEmpleados[5] = {
	"Garcia",
	"Perez",
	"Pinkas",
	"Jobs",
	"Musk"
};

const TipoTramite tiposTramite[3] = {
	{0, "Salario"},
	{1, "Vacaciones"},
	{2, "Cobertura Medica"}
};

int numeroRandom(int minimo, int maximo) {
	random_device rd;
	mt19937 generator(rd());
	uniform_int_distribution<int> distribution(minimo, maximo);

	return distribution(generator);
}

void crearEntornoTest (Tramite t[MAX_TRAMITES], int cantidad) {
	if (cantidad <= MAX_TRAMITES) {
		for (int i = 0; i < cantidad; i++) {
			string nombre = nombresEmpleados[numeroRandom(0,4)];
			string apellido = apellidosEmpleados[numeroRandom(0,4)];
			string apenom = nombre + " " +  apellido;
			// Pueden producirse colisiones
			int legajo = 100 + numeroRandom(100, 10000);

			Empleado e = {apenom, legajo};
			TipoTramite tt = tiposTramite[numeroRandom(0,2)];

			t[i].e = e;
			t[i].t = tt;
		}
	} else {
		printf("[ERROR] La cantidad de tramites no puede ser mayor a %d\n", MAX_TRAMITES);
	}
}

int legajoMasBajo (Tramite t[MAX_TRAMITES], int cantidad) {
	int bajo = 0;

	for (int i = 0; i < cantidad; i++) {
		if (i == 0) {
			bajo = i;
		} else {
			if (t[i].e.legajo < t[bajo].e.legajo) {
				bajo = i;
			}
		}
	}
	return bajo;
}

void mostrarResumen (Tramite t[MAX_TRAMITES], int cantidad) {
	for (int i = 0; i < cantidad; i++) {
		printf("# Tramite\n");
		printf(">>> Nombre: %s\n", t[i].e.nombre.c_str());
		printf(">>> Legajo: %d\n", t[i].e.legajo);
		printf(">>> Tramite: %s\n", t[i].t.descripcion.c_str());
	}
}

void ordenarPorTipoDeTramite(Tramite t[MAX_TRAMITES], int cantidad) {
	int i = 0;
	int j;
	Tramite aux;
	bool ordenado = false;

	while (i < cantidad && !ordenado) {
		ordenado = true;

		for (j = 0; j < cantidad - i - 1; j++) {
			if (t[j].t.tipo < t[j + 1].t.tipo) {
				aux = t[j];
				t[j] = t[j + 1];
				t[j + 1] = aux;
				ordenado = false;
			}
		}
		i++;
	}
}

void cantidadTramitesPorTipo (Tramite t[MAX_TRAMITES], int cantidad) {
	int i = 0;
	int key;

	while (i < cantidad) {
		key = t[i].t.tipo;
		string descripcion = t[i].t.descripcion;

		int contador = 0;

		while (i < cantidad && t[i].t.tipo == key) {
			contador += 1;
			i++;
		}
		printf(">>> La cantidad de tramites por %s fue de %d\n", descripcion.c_str(), contador);
	}
}
