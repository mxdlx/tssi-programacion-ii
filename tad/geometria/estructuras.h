#include <cmath>
#include <iostream>
using namespace std;

class Figura {
	public:
		virtual float area() = 0;
		virtual float perimetro() = 0;
};

class Cuadrado: public Figura {
	public:
		void setLado (float l) {
			lado = l;
		};
		float area () {
			return (float) pow(lado, 2);
		};
		float perimetro () {
			return lado * 4;
		};
	protected:
		float lado = 0;
};

class Circulo: public Figura {
	public:
		void setRadio (float r) {
			radio = r;
		};
		float area () {
			return M_PI * (float) pow(radio, 2);
		};
		float perimetro () {
			return 2 * M_PI * radio;
		};
	protected:
		float radio = 0;
};

class TrianguloEquilatero: public Figura {
	public:
		void setBase (float b) {
			base = b;
		};
		void setLado (float l) {
			lado = l;
		};
		float area () {
			double altura = sqrt(pow(lado, 2) - pow(0.5 * base, 2));
			return base * (float) altura / 2;
		};
		float perimetro () {
			return 2 * lado + base;
		};
	private:
		float lado = 0;
		float base = 0;
};

// Mas duras que la realidad
// Pentagono REGULAR
class Pentagono: public Figura {
	public:
		void setLado (float l) {
			lado = l;
		};
		float area () {
			float a = 0;
			return 5 * lado * apotema() / 2;
		};
		float perimetro () {
			return 5 * lado;
		};
	private:
		float lado = 0;
		float apotema () {
			return lado / 1.45;
		};
};

class Esfera: public Circulo {
	public:
		float volumen () {
			return 4/3 * M_PI * radio;
		};
};

class Cilindro: public Circulo {
	public:
		void setAltura(float a) {
			altura = a;
		}
		float volumen () {
			return M_PI * (float) pow(radio, 2) * altura;
		};
	private:
		float altura = 0;
};
